import biblioFlask 
from flask import Flask, session, request, redirect, url_for, render_template
import os
import random

# START BiblioFlask
BF = biblioFlask.BiblioFlask()
BF.dbFile = 'didepe.db'
BF.dirFiles = "static/files/"
BF.tables = {
                'documents' :{
                            'id': 'INTEGER PRIMARY KEY',
                            'title': 'TEXT',
                            'author': 'ARRAY',
                            'date': 'TEXT',
                            'language': 'TEXT',
                            'doctype': 'TEXT',
                            'description': 'TEXT',
                            'icone': 'TEXT',
                            'file': 'TEXT',
                            'user': 'TEXT',
                            },
                'collections': {
                             'id' : 'INTEGER PRIMARY KEY',
                             'title' :  'TEXT',
                             'description' :  'TEXT'
                },
                'authors': {
                             'id' : 'INTEGER PRIMARY KEY',
                             'name' :  'TEXT',
                             'surname' :  'TEXT'
                }}


# START Flas
app = Flask(__name__, static_url_path='/static')

@app.route('/', methods=['POST', 'GET'])
def index():
    items = {}
    items = BF.getAll('documents', '*')
    print(items)
    return render_template('index.html', rows=items)

@app.route('/monique')
def apropos():
    return render_template('monique.html')

@app.route('/form')
def formulaire():
    return render_template('form.html')

@app.route('/add-doc', methods=['GET', 'POST'])
def addDoc():
    if request.method == 'POST':
        BF.add('documents', request)
        return redirect(url_for('index'))
    else:
        return redirect(url_for('index'))

@app.route('/chercher')
@app.route('/chercher/<id_item>')
def chercher(id_item=-1):

    if int(id_item) > -1:
        item = BF.get('documents', id_item, '*')
    else:
        item=False

    rows = BF.getAll('documents', '*')
    return render_template("chercher.html", rows=rows, item=item)

@app.template_filter('shuffle')
def filter_shuffle(seq):
    try:
        result = list(seq)
        random.shuffle(result)
        return result
    except:
        return seq

@app.route('/remove/<table>/<id_item>')
def remove(table,id_item):
    try:
        BF.remove(table, id_item)
        return redirect(url_for('index'))
    except:
        print('error')

@app.route('/form_modify/<table>/<id_item>')
def form_modify(table,id_item):
    try:
        return redirect(url_for('index', action='modify', table=table, id_item=id_item))
    except:
        print('error')
    
@app.route('/modify', methods=['POST'])
def modify():
    if request.method == 'POST':
        BF.modify('documents', request.form.to_dict())
    return redirect(url_for('index'))

app.run(debug=True)