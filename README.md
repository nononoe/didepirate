La bibliothèque pirate du Didepe 

## install BiblioFlask
```
pip install git+https://gitlab.com/Luuse/recherche_formes-archives/biblioflask.git
```

## ressources
pour faire tourner l'app
```
$ make serve
```

# titres potentiels :

« Alors on met toutes les affaires, les livres les pommes les couteaux les cartes à jouer dans la partie de la cabane qui est encore un abri. » (abbregé : "Les livres les pommes les cartes à jouer")

« On lira des tas de pages sans s’arrêter. »

« On lit tout fort des phrases entières dans le livre »